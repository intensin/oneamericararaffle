import {useState} from 'react'
import {BrowserRouter,Routes, Route,Navigate} from "react-router-dom";
import {Registration} from "../Components/Registration";
import {Home} from "../Components/Home";
import {Formulario, ModaInfo, Radio, Notification, MinimsInfo} from "../Components/Registration/Interfaces";
import { User } from "../Components/Registration/Connection/Interfaces";
import {Minims} from "../Components/Minims";
interface NavigationsPros {
    crypto?:Radio[];
    formulario:Formulario;
    modal:ModaInfo;
    modalError:Notification;
    modalMissing:Notification;
    SetUser:(user:User) => void;
    user:User;
    minimsInfo:MinimsInfo;
}
export const Navigations = ({crypto,formulario,modal,modalError,modalMissing,SetUser,user,minimsInfo}:NavigationsPros) => {
    const [referralCode,setReferralCode] = useState<string>('');
    return (
        <BrowserRouter>
            <Routes>
                <Route
                    path="/register/:code"
                    element={
                        <Registration
                            crypto={crypto}
                            referralCode={referralCode}
                            setReferralCode={setReferralCode}
                            formulario={formulario}
                            modalError={modalError}
                            modalMissing={modalMissing}
                            SetUser={SetUser}
                            user={user}
                        />
                    }
                />
                <Route
                    path="/register"
                    element={
                        <Registration
                            crypto={crypto}
                            referralCode={referralCode}
                            setReferralCode={setReferralCode}
                            formulario={formulario}
                            modalError={modalError}
                            modalMissing={modalMissing}
                            SetUser={SetUser}
                            user={user}
                        />
                    }
                />
                <Route
                    path="home"
                    element={
                        <Home
                            modal={modal}
                            user={user}
                            referralCode={referralCode}
                            formulario={formulario}
                            
                        />
                    }
                />
                <Route
                    path="minims"
                    element={
                        <Minims
                            minimsInfo={minimsInfo}
                        />
                    }
                />
                <Route path="*" element={<Navigate to={'/register'}/>}/>
            </Routes>
        </BrowserRouter>
    )
}