import React, {FC, useEffect} from 'react';
import './App.css';
import {Layout,ConfigProvider} from 'antd';
import {Navigations} from "./routes/Navigations";
import { useLocale } from './Components/Registration/Hooks/useLocale';
import ReactGA  from 'react-ga';
import 'antd/dist/antd.css';
import {Idioms} from "./Components/Registration/Components/idioms";
import {ID,URL} from "./Settings/env";

const { Content } = Layout;

const App:FC = () => {
	const { locale,changeLocale,crypto,formulario,modal,modalError,modalMissing,SetUser,user,minimsInfo } = useLocale();
	useEffect(() => {
		ReactGA.initialize(ID);
		ReactGA.pageview(URL);
	}, []);
	
	return (
		<Layout className='app'>
			<ConfigProvider
				locale={locale}
			>
				<Content style={{height:'100vh'}}>
					<Idioms locale={locale} changeLocale={changeLocale}/>
					<Navigations
						crypto={crypto}
						modal={modal}
						formulario={formulario}
						modalError={modalError}
						modalMissing={modalMissing}
						SetUser={SetUser}
						user={user}
						minimsInfo={minimsInfo}
					/>
				</Content>
			</ConfigProvider>
		</Layout>
	);
}

export default App;
