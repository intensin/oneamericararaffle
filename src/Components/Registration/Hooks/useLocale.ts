import { useEffect, useState } from "react";
import { RadioChangeEvent } from "antd";
import moment from "moment";
import esEs from "antd/lib/locale/es_ES";
import enUS from "antd/lib/locale/en_US";
import { Formulario, ModaInfo, Radio, Notification, MinimsInfo } from "../Interfaces";
import {
	FormualrioEs, mailMessageEs,
	minimsEs,
	ModaEs,
	notificationEs,
	notificationFaltanteEs,
	RadioEs,
} from "../../../Settings/Locales/esEs";
import {
	FormualrioEn,
	mailMessageEn,
	minimsEn,
	ModaEn,
	notificationEn,
	notificationFaltanteEn,
	RadioEn,
} from "../../../Settings/Locales/enUs";
import { User } from "../Connection/Interfaces";

const use: User = {
	tradeCrypto: false,
	phone: "",
	name: "",
	email: "",
	createdAt: new Date(),
	code: "",
};
export const useLocale = () => {
	const [locale, setLocale] = useState(enUS);
	const [crypto, setCrypto] = useState<Radio[]>();
	const [formulario, setFormulario] = useState<Formulario>(FormualrioEs);
	const [modal, setModal] = useState<ModaInfo>(ModaEs);
	const [modalError, setModalError] = useState<Notification>(notificationEs);
	const [modalMissing, setModalMissing] = useState<Notification>(notificationFaltanteEs);
	const [user, setUser] = useState<User>(use);
	const [minimsInfo, setMinimsInfo] = useState<MinimsInfo>(minimsEs);
	const [mailMessage,setMailMessage] = useState<string>(mailMessageEs);
	function detectLanguage() {
		return navigator.languages && navigator.languages.length ? navigator.languages[0] : navigator.language;
	}
	
	useEffect(() => {
		CryptoSet();
		
		const defaultLanguage = detectLanguage();
		//first version of autolanguge detection
		try {
			if (defaultLanguage.indexOf("es") !== -1) {
				setLocale(esEs);
			}
		} catch (e) {}
	}, []);
	
	useEffect(() => {
		CryptoSet();
	}, [locale]);
	
	const changeLocale = (e: RadioChangeEvent) => {
		const localeValue = e.target.value;
		setLocale(localeValue);
		
		if (!localeValue) {
			moment.locale("es");
		} else {
			moment.locale("en");
		}
	};
	
	const SetUser = (user: User) => {
		setUser(user);
	};
	
	const CryptoSet = () => {
		let data: Radio[];
		let formu: Formulario;
		let Modal: ModaInfo;
		let ModalError: Notification;
		let ModalMissing: Notification;
		let Minims: MinimsInfo;
		let MailMessage:string;
		if (locale === esEs) {
			formu = FormualrioEs;
			data = RadioEs;
			Modal = ModaEs;
			ModalError = notificationEs;
			ModalMissing = notificationFaltanteEs;
			Minims = minimsEs;
			MailMessage = mailMessageEs;
		} else {
			formu = FormualrioEn;
			data = RadioEn;
			Modal = ModaEn;
			ModalError = notificationEn;
			ModalMissing = notificationFaltanteEn;
			Minims = minimsEn;
			MailMessage = mailMessageEn;
		}
		setModal(Modal);
		setFormulario(formu);
		setCrypto(data);
		setModalError(ModalError);
		setModalMissing(ModalMissing);
		setMinimsInfo(Minims);
		setMailMessage(MailMessage);
	};
	
	return {
		locale,
		crypto,
		formulario,
		modal,
		modalError,
		modalMissing,
		user,
		minimsInfo,
		mailMessage,
		changeLocale,
		CryptoSet,
		SetUser,
	};
};
