import {Locale} from "antd/es/locale-provider";
import {RadioChangeEvent} from "antd";
import {minimsEn} from "../../../Settings/Locales/enUs";
import { User } from "./../Connection/Interfaces";

export interface Radio {
	label: string;
	value: boolean;
}

export interface Notification{
	message:string;
	description:string;
}

export interface Formulario {
	email:string;
	emailPlaceholder:string;
	phone:string;
	phonePlaceholder:string;
	name:string;
	namePlaceholder:string;
	question:string;
	button:string;
	legend:string;
	title:string;
	text:string;
	introText:string;
	shareAction:string,
	subText:string;
	requiredActiverCompartir:string;
	requiredEmail:string;
	linkinstruction:string;
	linkinstruction2:string;
	linkinstruction3:string;
	invitationfirst:string;
	invitationsecond:string;
	gobacktofirstpage:string;

}

export interface RegistrationFormPros {
	formulario?:Formulario;
	crypto?:Radio[];
	referralCode:string;
	RegisterMail: () => void;
	activerCompartir:boolean;
	Comaprtir: (onFin:boolean) => void;
	requiredActiverCompartir:boolean;
	requiredEmail:boolean;
	errorEmail:string;
	user:User,
	searching:boolean
	SetUser:Function,
}

export interface IdiomsPros {
	locale:Locale;
	changeLocale: (e: RadioChangeEvent) => void;
}

export interface UserFormulario {
	email:string;
	phone:string;
	name:string;
	question:boolean;
}

export interface ModaInfo {
	share:string;
	titleModal:string;
	title:string;
	paragraph:string
}

export interface MinimsInfo {
	title:string;
	name:string;
	email:string;
	numberOfReferrals:string;
	nextButton:string;
	previousButton:string;
	downloadButton:string;
}