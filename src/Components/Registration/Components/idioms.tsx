import esEs from "antd/lib/locale/es_ES";
import enUS from "antd/lib/locale/en_US";
import {Image, Radio} from "antd";
import { IdiomsPros } from "../Interfaces";
import logo from "../../../oneamericas.png"


export const Idioms = ({locale,changeLocale}:IdiomsPros) =>{
	return (
		<>
			<div id="header" style={{paddingTop: '20px',paddingRight:'20px',display:"flex",
				justifyContent: "flex-end",
				right:"0",zIndex:"2"}}>
				{/* <div>
				<h2 style={{color:"white",fontWeight:'bold'}}>
THE ONE AMERICAS</h2>	
<h2 style={{color:"white",fontWeight:'',fontSize:'2em',fontFamily:'Brush Script MT'}}>
Great Giveaway</h2>	
</div> */}
				<Radio.Group value={locale} onChange={changeLocale} style={{ padding: '0 50px' }}>
					<Radio.Button key="en" value={enUS}>
						EN
					</Radio.Button>
					<Radio.Button key="es" value={esEs}>
						ES
					</Radio.Button>
				</Radio.Group>
				{<img src={logo} style={{height:"140px",objectFit:"cover",objectPosition:'right'}}/>}
			</div>
			
		</>
		
	)
}