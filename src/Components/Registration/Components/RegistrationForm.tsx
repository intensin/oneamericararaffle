import { useState } from "react";
import { Button, Col, Row, Form, Input, Radio, Space } from "antd";
import { Typography } from "antd";
import { WhatsAppOutlined } from "@ant-design/icons";
import { RegistrationFormPros } from "../Interfaces";
import { URLWH } from "../../../Settings/env";

import "react-phone-number-input/style.css";
import PhoneInput from "react-phone-number-input";

const { Title, Text } = Typography;

export const  RegistrationForm = ({
	                                  formulario,
	                                  crypto,
	                                  referralCode,
	                                  RegisterMail,
	                                  searching,
	                                  activerCompartir,
	                                  Comaprtir,
	                                  requiredActiverCompartir,
	                                  requiredEmail,
	                                  errorEmail,
	                                  user,
	                                  SetUser
                                  }: RegistrationFormPros) => {
	const [value, setValue] = useState<string>();
	const Compartir = () => {
		Comaprtir(true);
		const tagA = document.createElement("a");
		tagA.href = URLWH + user?.name + " " +formulario?.invitationfirst + " " + referralCode;
		tagA.target = "_blank";
		tagA.click();
	};
	return (
		<Col span={24} style={{ padding: '0 16px' }}>
			<Title level={3} style={{ fontFamily: 'Copperplate, "Copperplate Gothic Light", fantasy', textAlign: "center" }}>
				{formulario?.title}
			</Title>
			<Space size={0} direction="vertical" style={{ textAlign: "center" }}>
				<Text>{formulario?.text}</Text>
				<br />
				<Text>{formulario?.introText}</Text>
				<Title
					level={4}
					style={{ fontFamily: 'Copperplate, "Copperplate Gothic Light", fantasy', textAlign: "center" }}
				>
					{formulario?.subText}
				</Title>
				
				<Form.Item
					label={formulario?.email}
					name={"email"}
					rules={[{ required: true, type: "email", message: formulario?.requiredEmail }]}
					// help={searching?<Spin spinning={searching} tip="Validating this email..." />:""}
				>
					<Input placeholder={formulario?.emailPlaceholder} onBlur={() => RegisterMail()} />
				
				</Form.Item>
				{requiredEmail && (
					<Row>
						<Col>
							<label style={{ color: "#ff4d4f", textAlign: "center" }}>* {errorEmail}</label>
						</Col>
					</Row>
				)}
				<Form.Item name="phone" label={formulario?.phone} extra="Select your country first">
					<PhoneInput defaultCountry="US" placeholder="Enter phone number" value={value} onChange={(value: string) => setValue(value)} />
					{/* <Input placeholder={formulario?.phonePlaceholder} style={{ width: "100%" }} /> */}
				</Form.Item>
				<Form.Item label={formulario?.name} name={"name"}>
					<Input placeholder={formulario?.namePlaceholder} onChange={(e)=>{SetUser({...user,name:e.target.value})}} />
				</Form.Item>
				
				<Form.Item label={formulario?.question} name={"question"}>
					<Radio.Group options={crypto} defaultValue={false} />
				</Form.Item>
				
				<Row>
					<Col span={3}>
						<Form.Item>
							<Button disabled={activerCompartir} onClick={Compartir} shape="circle" size={"large"}>
								{activerCompartir ? (
									<WhatsAppOutlined />
								) : (
									<WhatsAppOutlined style={{ fontSize: "30px", color: "green" }} />
								)}
							</Button>
						</Form.Item>
					</Col>
					<Col span={21} style={{ color: "", textAlign: "left", paddingLeft: "10px" }}>
						{/* <a onClick={Compartir} href=""> */}
						<label style={{ color: "", textAlign: "left" }}>{formulario?.legend}</label>
						<br />
						{referralCode && (
							<a onClick={Compartir} >{formulario?.shareAction}:{referralCode} </a>
						)}
					</Col>
				</Row>
				{requiredActiverCompartir && (
					<Row>
						<Col>
							<label style={{ color: "#ff4d4f", textAlign: "center" }}>* {formulario?.requiredActiverCompartir}</label>
						</Col>
					</Row>
				)}
				<Form.Item style={{ marginTop: "3em" }}>
					<Button type="primary" htmlType="submit" block size={"large"}>
						{!requiredActiverCompartir
							? formulario?.button+' & '+formulario?.shareAction
							:formulario?.button}
					</Button>
				</Form.Item>
			</Space>
		</Col>
	);
};
