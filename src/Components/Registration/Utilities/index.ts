export const validateMessages = {
	required: '${label} is required!',
	types: {
		email: '${label} is not a valid email!',
		number: '${label} is not a valid number!',
	},
	number: {
		range: '${label} must be between ${min} and ${max}',
	},
};

export function utf8_to_b64( str:string ) {
	return window.btoa(unescape(encodeURIComponent( str )));
}