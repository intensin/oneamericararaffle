import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import { collection, addDoc,doc,getDocs,query,where,updateDoc,getDoc } from "firebase/firestore";
import {User, Referral, Email} from "./Interfaces";
import {APIKEY, APPID, MESSAGINGSENDERID, PROJECTID} from "../../../Settings/Firebase";

const firebaseConfig = {
	apiKey: APIKEY,
	authDomain: PROJECTID+".firebaseapp.com",
	projectId: PROJECTID,
	storageBucket: PROJECTID+".appspot.com",
	messagingSenderId: MESSAGINGSENDERID,
	appId: APPID
};

const app = initializeApp(firebaseConfig);
export const DB = getFirestore(app);

export const getUser = async (code?:string) => {
	if (code === undefined){
		return null;
	}
	let codigo:string|null = null;
	let email:string|null = null;
	const documento = collection(DB,'users');
	const q = query(documento, where("code", "==", code));
	const data = await getDocs(q);
	data.forEach((d)=>{
		codigo = d.id;
		let datos = d.data();
		email = datos.email;
	})
	return {codigo:codigo,email:email};
}
export const AddUSer = async (user:User) => {
	const coleccion = collection(DB,'users');
	const Usuario = await addDoc(coleccion,user);
	return Usuario.id;
}
export const AddEmail = async (email:Email) => {
	const coleccion = collection(DB,'emails');
	const Correo = await addDoc(coleccion,email);
	return Correo.id;
}
export const Referrals = async (user:Referral) => {
	const coleccion = collection(DB,'referrals');
	const documento = doc(DB,'users',user.idUserReferrals);
	const users =  await getDoc(documento);
	let data;
	if(users.exists()){
		data = users.data();
		let cantidad = data.cantidad;
		cantidad = (cantidad === undefined) ? 0 : cantidad;
		await updateDoc(documento,{cantidad:parseInt(cantidad)+1});
	}
	
	await addDoc(coleccion,user);
}
export const SearchMail = async (email:string) => {
	const coleccion = collection(DB,'emails');
	const emails = await getDocs(coleccion);
	let encontrado:boolean = false;
	const info = emails.docs.map(doc => doc.data())
	info.forEach((e)=>{
		if (e.correo === email) encontrado = true;
	});
	return encontrado;
}
export const ActivateMail = async (code:string) => {
	let codigo = code?? '';
	const coleccion = doc(DB,'emails',codigo);
	await updateDoc(coleccion,{compartir:true});
}