import { Form, Row } from "antd";
import { Navigate, useParams } from "react-router-dom";
import { RegistrationForm } from "./Components/RegistrationForm";
import { Email, Referral, User } from "./Connection/Interfaces";
import { ActivateMail, AddEmail, AddUSer, getUser, Referrals, SearchMail } from "./Connection";
import { useEffect, useRef, useState } from "react";
import { Formulario, Radio, UserFormulario, Notification } from "./Interfaces";
import { validateMessages } from "./Utilities";
import {SERVICE_ID, TEMPLATE_ID, URL, URLWH, USER_ID} from "../../Settings/env";
import emailjs from '@emailjs/browser';

interface RegistrationPros {
	crypto?: Radio[];
	formulario: Formulario;
	modalError: Notification;
	modalMissing: Notification;
	referralCode: string;
	setReferralCode: Function;
	
	SetUser: (user: User) => void;
	user: User,
}

export const Registration = ({
	                             referralCode,
	                             setReferralCode,
	                             crypto,
	                             formulario,
	                             modalError,
	                             modalMissing,
	                             SetUser,
	                             user,
                             }: RegistrationPros) => {
	const code = useParams();
	const [form] = Form.useForm<UserFormulario>();
	const formRef = useRef<HTMLFormElement | null>(null);
	const [userReferrals, setUserReferrals] = useState<string | null>(null);
	const [email,setEmail] = useState<string>('');
	const [newCode, setNewCode] = useState<string>("");
	const [isModalVisible, setIsModalVisible] = useState<boolean>(false);
	const [activerCompartir, setActiverCompartir] = useState<boolean>(true);
	const [activerRegistrar, setActiverRegistrar] = useState<boolean>(true);
	const [searching, setSearching] = useState<boolean>(false);
	const [requiredActiverCompartir, setRequiredActiverCompartir] = useState<boolean>(false);
	const [requiredEmail, setRequiredEmail] = useState<boolean>(false);
	
	useEffect(() => {
		const codigo = code.code;
		if (codigo !== undefined) {
			const optenerUser = async () => {
				let cod = await getUser(codigo);
				if (cod){
					setUserReferrals(cod.codigo);
					if (cod.email) setEmail(cod.email);
				}
			};
			optenerUser().then();
		}
	}, []);
	const RegisterMail = async () => {
		const values = await form.validateFields();
		setSearching(true);
		const encontrado = await SearchMail(values.email);
		if (encontrado) {
			setRequiredEmail(true);
			setSearching(false);
		} else {
			const data: Email = { correo: values.email, compartir: false };
			let referralCode = await AddEmail(data);
			setNewCode(referralCode);
			setRequiredEmail(false);
			referralCode = URL + referralCode;
			setActiverCompartir(false);
			setReferralCode(referralCode);
			setSearching(false);
		}
	};
	const newPage = () => {
		const tagA = document.createElement("a");
		tagA.href = URLWH + user?.name + " " +formulario?.invitationfirst + " " + referralCode;
		tagA.target = "_blank";
		tagA.click();
	}
	const Share = async (onFin:boolean) => {
		await ActivateMail(newCode);
		setRequiredActiverCompartir(false);
		setActiverRegistrar(false);
		if (onFin) await onFinis(false,false);
	};
	const onFinis = async (onSare:boolean,page:boolean) => {
		const values = await form.validateFields();
		let user: User;
		if (requiredEmail) return null;
		if (onSare){
			if (activerRegistrar) await Share(false);
		}
		let date: Date = new Date();
		user = {
			tradeCrypto: values.question?? false,
			phone: values.phone,
			name: values.name,
			email: values.email,
			createdAt: date,
			code: newCode,
		};
		const id = await AddUSer(user);
		if (userReferrals) {
			let dato: Referral = {
				createdAt: date,
				idUsers: id,
				idUserReferrals: userReferrals,
			};
			await Referrals(dato);
			let data: HTMLFormElement | string;
			if (formRef.current){
				data = formRef.current;
			}else {
				data = '';
			}
			emailjs.sendForm(SERVICE_ID,TEMPLATE_ID,data,USER_ID)
				.then((result)=>{
					console.log('Correo enviado',result);
				})
				.catch((error)=>{
					console.error(error);
				})
		}
		SetUser(user);
		setIsModalVisible(true);
		if (page){
			newPage();
		}
	};
	if (isModalVisible) return <Navigate to={"/home"} />;
	return (
		<>
			<div >
				<>
					<Form form={form} layout="vertical" validateMessages={validateMessages} size={"small"} onFinish={() =>onFinis(true,true)}>
						<Row align={"middle"} justify={"center"} >
							<RegistrationForm
								formulario={formulario}
								crypto={crypto}
								referralCode={referralCode}
								RegisterMail={RegisterMail}
								activerCompartir={activerCompartir}
								Comaprtir={Share}
								requiredActiverCompartir={requiredActiverCompartir}
								requiredEmail={requiredEmail}
								errorEmail={modalError?.description}
								user={user}
								searching={searching}
								SetUser={SetUser}
							/>
						
						</Row>
					</Form>
					{/* <a target="_blank" href="/privacy policy one americas.pdf">Privacy Policy</a> */}
				</>
				<form ref={formRef} style={{display:'none'}}>
					<input type="text" name="email" value={email} />
					<input type="text" name="from_name" value='The one america raffle to unify all america' />
					<textarea name="message" value='Congratulations a new participant registered with your code'/>
				</form>
			</div>
		</>
	);
};
