import { User } from "../Registration/Connection/Interfaces";
import { ModaInfo } from "../Registration/Interfaces";
import { Card, Row, Typography } from "antd";
import { URLWH } from "../../Settings/env";
import { WhatsAppOutlined } from "@ant-design/icons";
import { Formulario } from "./../Registration/Interfaces";
import {Link} from "react-router-dom";
const { Title, Text } = Typography;

interface HomePros {
  modal: ModaInfo;
  user: User;
  referralCode: string;
  formulario: Formulario;
}
export const Home = ({ modal, user, referralCode,formulario }: HomePros) => {
  return (
    <>
      {" "}
      <div >
        <Row align={"middle"} justify={"center"} gutter={{ xs: 24, sm: 24, md: 14 }}>
          <Title>{modal.title}</Title>
          <Card style={{ marginTop: 16, width: "100%", textAlign: "center" }} title={user.name}>
            <h2>{modal.titleModal}</h2>

            <Text>
              {modal.paragraph}
              <br /> <br />
              <b>{modal.share}</b> : {user.name}
            </Text>
            <br />
            <br />

          {referralCode  &&  <Text>
              <p>{formulario?.linkinstruction3} </p>

              <a target="_blank" href={referralCode}>
                {referralCode}{" "}
              </a>
			  <br />
			  <br />
              <p>
{formulario?.linkinstruction}
              </p>
              <p>{formulario?.linkinstruction2}</p>
              <a target="_blank" href={URLWH + user?.name+ " "+formulario?.invitationfirst  + " " + referralCode}>
                <WhatsAppOutlined style={{ fontSize: "30px", color: "green" }} />
              </a>
            </Text>
			}
          </Card>
        </Row>

		<Row align={"middle"} justify={"center"} gutter={{ xs: 24, sm: 24, md: 14 }}>
		<Link to="/register">{formulario?.gobacktofirstpage}</Link>

			</Row>
      </div>
    </>
  );
};
