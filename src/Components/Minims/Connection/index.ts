import {
	collection,
	query,
	orderBy,
	startAfter,
	limit,
	getDocs,
	QueryDocumentSnapshot,
	DocumentData,
	where
} from "firebase/firestore";

import {DB} from '../../Registration/Connection';

interface Tabla {
	id:string;
	name:string;
	email:string;
	numberOfReferrals:number;
}

export const getAllUsers = async () => {
	let datos:Tabla[]|undefined;
	const first = query(
		collection(DB, "users"),
		orderBy("createdAt")
	);
	const documentSnapshots = await getDocs(first);
	documentSnapshots.docs.map((doc) => {
		const documeto = doc.data();
		if (datos === undefined){
			datos = [{
				id: doc.id,
				name: documeto.name,
				email: documeto.email,
				numberOfReferrals: (documeto.cantidad === undefined) ? 0 : documeto.cantidad
			}];
		}else {
			datos = datos.concat({
				id: doc.id,
				name: documeto.name,
				email: documeto.email,
				numberOfReferrals: (documeto.cantidad === undefined) ? 0 : documeto.cantidad
			});
		}
	});
	return datos;
}

export const obtenerCantidad = async (code:string) => {
	const Collection = collection(DB,'referrals');
	const Referral = query(Collection,where('idUserReferrals','==',code));
	const documentos = await getDocs(Referral);
	return documentos.docs.length;
}

export const buscarSinlimites = async () => {
	let datos:Tabla[]|undefined;
	const first = query(
		collection(DB, "users"),
		orderBy("createdAt")
	);
	const documentSnapshots = await getDocs(first);
	documentSnapshots.docs.map(async (doc) => {
		const documeto = doc.data();
		const cantidad = 0;
		if (datos === undefined){
			datos = [{
				id: doc.id,
				name: documeto.name,
				email: documeto.email,
				numberOfReferrals: cantidad
			}];
		}else {
			datos = datos.concat({
				id: doc.id,
				name: documeto.name,
				email: documeto.email,
				numberOfReferrals: cantidad
			});
		}
	});
	return datos;
}
export const anterior = async (firstVisible:QueryDocumentSnapshot<DocumentData>|null) => {
	let datos:Tabla[]|undefined;
	const first = query(
		collection(DB, "users"),
		orderBy("createdAt"),
		startAfter(firstVisible),
		limit(100)
	);
	const documentSnapshots = await getDocs(first);
	const document = documentSnapshots.docs.reverse();
	document.map((doc) => {
		const documeto = doc.data();
		const cantidad = 0;
		if (datos === undefined){
			datos = [{
				id: doc.id,
				name: documeto.name,
				email: documeto.email,
				numberOfReferrals: cantidad
			}];
		}else {
			datos = datos.concat({
				id: doc.id,
				name: documeto.name,
				email: documeto.email,
				numberOfReferrals: cantidad
			});
		}
	});
	return {documentos: datos,
		lastVisible: document[documentSnapshots.docs.length-1],
		firstVisible: document[0]
	};
}