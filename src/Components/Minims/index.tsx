import {Button, PageHeader, Spin, Table, Descriptions } from "antd";
import {useEffect, useState} from "react";
import { getAllUsers} from "./Connection";
import Column from "antd/lib/table/Column";
import {MinimsInfo} from "../Registration/Interfaces";
const ExportJsonExcel = require('js-export-excel');

interface Tabla {
	id:string;
	name:string;
	email:string;
	numberOfReferrals:number;
}
interface MinimsPros {
	minimsInfo:MinimsInfo
}
export const Minims = ({minimsInfo}:MinimsPros) => {
	
	const [ users, setUsers ] = useState<Tabla[]>();
	const [ searching, setSearching ] = useState<boolean>(false);
	const [ directRecords, setDirectRecords] = useState<number>(0);
	const [ referredRecords, setReferredRecords] = useState<number>(0);
	const [ search, setSearch ] = useState<boolean>(false);
	useEffect(()=>{
		getInformation().then();
	},[]);
	useEffect(()=>{
		knowNumberOfUsers()
	},[users]);
	
	const getInformation = async () => {
		setSearching(true);
		const data = await getAllUsers();
		if (data){
			setUsers(data);
		}
		setSearching(false);
	}
	const knowNumberOfUsers = () => {
		const data = users;
		let directRecords:number = 0;
		let referredRecords:number = 0;
		data?.forEach((user)=>{
			referredRecords += user.numberOfReferrals;
			directRecords++;
		});
		setDirectRecords(directRecords - referredRecords);
		setReferredRecords(referredRecords);
	}
	const downloadInformation = async () => {
		setSearch(true);
		let option = {
			fileName: "excel",
			datas: [
				{
					sheetData: users,
					sheetName: "sheet",
					sheetFilter: ["name", "email", "numberOfReferrals"],
					sheetHeader: ["name", "email", "numberOfReferrals"],
					columnWidths: [10, 20, 15],
				}
			]
		};
		let toExcel = new ExportJsonExcel(option);
		toExcel.saveExcel();
		setSearch(false);
	}
	
	return (<>
		<div>
			<PageHeader
				title={minimsInfo.title}
				extra={[
					<Button onClick={downloadInformation} loading={search} >{minimsInfo.downloadButton}</Button>
				]}
			>
				<Descriptions size="small" column={2}>
					<Descriptions.Item label="Registros directos">{directRecords}</Descriptions.Item>
					<Descriptions.Item label="Registros referidos">{referredRecords}</Descriptions.Item>
				</Descriptions>
			</PageHeader>
			<Spin spinning={searching} tip="Validating this email...">
				<Table
					dataSource={users}
					scroll={{ x: 400 }}
					pagination={{ pageSize: 100,position: ['bottomCenter'] }}
					size='small'
					rowKey={record => record.id}
				>
					<Column
						title={minimsInfo.name}
						dataIndex='name'
						align='center'
					/>
					<Column
						title={minimsInfo.email}
						dataIndex='email'
					/>
					<Column
						title={minimsInfo.numberOfReferrals}
						dataIndex='numberOfReferrals'
						fixed='right'
						align='center'
					/>
				</Table>
			</Spin>
		</div>
	</>)
}