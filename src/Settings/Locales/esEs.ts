import { Formulario, MinimsInfo, ModaInfo, Radio } from "../../Components/Registration/Interfaces";

export const FormualrioEs: Formulario = {
  email: "Correo",
  emailPlaceholder: "Por favor escribe su Correo",
  phone: "Teléfono",
  phonePlaceholder: "Por favor escribe su Teléfono",
  name: "Nombre",
  namePlaceholder: "Por favor escribe su Nombre",
  question: "¿Negocia o mantiene cripto?",
  button: "Registrar",
  legend:
    "Comparte esta invitación a 3 o más de tus contactos usando en icono de WhatsApp\
	Por cada referido adicional recibirás un ticket adicional para el sorteo. \
	Por 10 o más referidos será automáticamente seleccionado para el obsequio.",
  title: "500 dólares para 5 millones de ganadores",
  text: "The One Americas está llevando a cabo el sorteo más grande de la historia, brindando a 100,000 personas la oportunidad de ganar 500 dólares cada una, cada semana durante 50 semanas",
  introText: 'Para participar llena la siguiente información y comparte este sitio con tu familia, amigos, conocidos. Por cada referido confirmado ganaras una boleta adicional para el sorteo. ',
  subText: "Regístrese a continuación para calificar",
  shareAction: 'Compartir',
  requiredActiverCompartir:
    "Debe compartir esta página utilizando el icono o enlace anterior antes de poder registrarse",
  requiredEmail: "El correo es requerido",
  linkinstruction:'Puedes copiar este enlace para compartirlo con tus contactos, entre más referidos más opciones de ganar.',
  linkinstruction2:'Ó usa este link de whatsapp para compartirlo con tus contactos.',
	linkinstruction3:'Si olvidaste compartir la página puedes usar el siguiente enlace para referidos',
  invitationfirst:" te ha invitado a participar en el sorteo de One Americas. \
	Puedes ser uno de los ganadores de $500 dolares. \
	Registrate ahora para ganar  :D \
	Buena suerte, solo registrate en la siguiente pagina",
	invitationsecond:'',
  gobacktofirstpage:'Volver a la página inicial'	
};
export const RadioEs: Radio[] = [
  { label: "Si", value: true },
  { label: "no", value: false },
];
export const ModaEs: ModaInfo = {
  share: "La mejor de las suertes",
  titleModal: "Registro exitoso",
  title: "Gracias",
  paragraph:
    "\
	Felicitaciones tu suscripción para el obsequio ha sido recibida correctamente. \
El primer sorteo se realizará el 4 de Marzo del 2022, 100,000 participantes recibirán $500 cada uno cómo  parte  del obsequio de One Americas,\
y semanalmente se realizara un sorteo adicional por 50 semanas\
\
Los mejores deseos para el sorteo y obsequios.",
};
export const notificationEs = {
  message: "Error",
  description: "lo sentimos este correo ya está registrado.",
};
export const notificationFaltanteEs = {
  message: "Error",
  description: "Falta compartir el enlace por WhatsApp.",
};
export const minimsEs: MinimsInfo = {
  title: "lista de usuarios registrados",
  name: "Nombre",
  email: "Correo",
  numberOfReferrals: "Número de referidos",
  nextButton: "Próxima",
  previousButton: "Anterior",
  downloadButton: 'Descargar'
};
export const mailMessageEs = 'Felicidades un nuevo participante se registró con tu codigo';
