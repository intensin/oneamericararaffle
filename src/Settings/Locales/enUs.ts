import {Formulario, MinimsInfo, ModaInfo, Radio} from "../../Components/Registration/Interfaces";

export const FormualrioEn:Formulario = ({
	email: 'Email', emailPlaceholder: 'Please write your Email',
	phone: 'Phone',phonePlaceholder: 'Please write your Phone',
	name: 'Name',namePlaceholder:'Please write your Name',
	question:'Do you Trade or Hold crypto?',button:'Register',
	legend:'Share this invitation to 3 or more contacts using the WhatsApp icon. \
	For each additional referral you will receive one additional ticket for the raffle. \
	For 10 or more referrals, you will be preselected for the giveaway.',
	title:'$500 for 5 million winners',
	text:'The One Americas is conducting the largest giveaway in history,\
	 giving 100,000 people a chance to win 500 Dollars each, every week for 50 weeks.',
	introText: 'In order to participate please first fill in  the following information and then share this page with your friends, family, contacts. For each additional confirmed referal you will earn a ticket for the raffle.',
	shareAction: 'Share',
	subText:'Register below to qualify',
	requiredActiverCompartir: 'You should share this page using the above icon or link before being able to register',
	requiredEmail:'The email is required',
	linkinstruction:'This link can  be copied to invite your friends to get more referals, remember with more referals you \
	have a greater chance of winning first.',
	linkinstruction2:'Or use this whatsapp link to share to your contacts',
	linkinstruction3:'If you forgot to invite someone this is your referal link',
	invitationfirst:" have invited you to join the One Americas raffle. \
	Be one of 5millon people to win $500. \
	Register now for a chance to win :D \
	 Good luck! Just register in the following page",
	invitationsecond:'x',	
	gobacktofirstpage:'Go back to first page'	
});

export const RadioEn:Radio[] = ([
	{label: 'Yes', value: true},
	{label: 'No', value: false}
])
//the following link , which includes your referral code,
export const ModaEn:ModaInfo = ({
	share: 'Best of luck',titleModal: 'Successful Registration',
	title: 'Thank you',
	paragraph: "Congratuations, your entry for the raffle has been successfully submitted.\
	The first raffle will be drawn on 4th March 2022. 100,000 participants will be receiving $500 each as part of The One Americas Giveaway, and weekly thereafter for 50 consecutive weeks.\
	\You can also share  with friends, family and acquaintances to accumulate additional tickets\
	\Best Wishes for The One Americas Giveaway."
});
export const notificationEn = ({
	message:'Error',description: 'sorry this email is already registered.'
});
export const notificationFaltanteEn = ({
	message:'Error',description: 'Share the link by WhatsApp.'
});

export const minimsEn:MinimsInfo = ({
	title:'list of registered users',name:'Name',email:'Email',numberOfReferrals:'Number of referrals',
	nextButton:'Next',previousButton: 'Previous',downloadButton:'Download'
});

export const mailMessageEn = 'Congratulations a new participant registered with your code';